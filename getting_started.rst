Getting started
=================================

Overview
---------------------------------

Partoo API is currently available for **partners** only. You will need an **API key** to query the API. You are not a partner yet but you would like to use our API? Please reach out to us at:

**support [at] partoo . fr**

Partoo API is accessible through a RESTful interface and all data is returned in JSON format. This documentation will guide through all resources made available via the API.


Authentification
---------------------------------

You should provide an API key to any request you make to the API. Provide your key directly in the request URL with the ``api_key`` parameter as follow:

.. code-block:: html

	GET https://partoo.fr/api/business/{business_id}?api_key={my_api_key}

