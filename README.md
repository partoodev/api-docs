# Requirements

## How to install sphinx on Ubuntu

``sudo apt-get install python-sphinx``

## How to install sphinx_rtd_theme

See https://github.com/snide/sphinx_rtd_theme
``sudo pip install sphinx_rtd_theme``

## Build the doc

``make html``


# Main things to know about rST

## How to write some code

With line numbers:

```
.. code-block:: javascript
    :linenos:

	{
	    'business': {
	    	'id': 'mon zboob'
	    }
	}
```

Without line numbers:

```
.. code-block:: javascript

	{
	    'business': {
	    	'id': 'mon zboob'
	    }
	}
```

In-line code sample:

``python setup.py``
