API resources
=================================

Business details
---------------------------------

Retrieve **all information** about a given **business**.

Request URL:

.. code-block:: html

    GET http://www.partoo.fr/api/business/{business_id}?api_key={my_api_key}


Query parameters:

==================  ==========  ===========  ==================
Parameter           Type        Required     Description
==================  ==========  ===========  ==================
api_key             string      True         Your API key
business_id         string      True         Id of the business
==================  ==========  ===========  ==================

JSON response:

.. code-block:: javascript

    {
        'id'                : business id (string),
        'code'              : business owners sometimes have their own ids to identify their businesses (string)
        'status'            : status of the business 'open' or 'closed' (string),
        'org_id'            : organization id (every client is identified by a unique id on Partoo) (int)
        'name'              : name of the business (string),
        'created'           : timestamp when the business was created (int),
        'modified'          : timestamp when the business was last modified (int),
        'address'           : address of the business (string),
        'address_details': {   components of the "address" field (e.g. "13 bis rue du Rosier")
            'number'              : street number (e.g. "13") (string)              
            'number_supplement'   : "bis" or "ter" (e.g. "bis") (string) 
            'street_type'         : type of street (e.g. "rue")
            'street_name'         : name of the street (e.g. "du Rosier")
        },
        'address2'          : address supplement (string),
        'city'              : city (string),
        'zipcode'           : zipcode (string),
        'region'            : region (string),
        'country'           : country (string) - ISO 3166 alpha2 code,
        'lat'               : latitude (float),
        'long'              : longitude (float),
        'siret'             : SIRET code (string),
        'categories'        : categories the business belong to (list of strings),
        'description_short' : short description (200 characters max) of the business activity (string),
        'description_long'  : long description (1000 characters max) of the business activity (string),
        'website_url'       : URL to the website of the business (string),
        'facebook_url'      : URL to the Facebook page (string),
        'twitter_url'       : URL to the Twitter feed (string),
        'logo_url'          : URL to the business logo (string),
        'photos': {
            'primary'   : URL to the picture that the business owner want to show primarily (string),
            'secondary' : URLs to secondary pictures (list of strings)
        },
        'videos'        : URLs to videos (list of strings),
        'contacts': [     List of contact objects
            {
                'name'            : name of the contact (string),
                'email'           : email of the contact (string),
                'phone_numbers'   : phone numbers of the contact (list of strings),
            }
        ],
        'open_hours': {
            // Different from human_friendly_open_hours for processing after midnight hours
            // e.g : For opening on Monday from 11:00 to 02:00  => 'monday': ['11:00-00:00']
            //                                                  => 'tuesday': ['00:00-02:00']
            'monday'     : Monday open hours (list of strings like ['07:30-10:00', '13:30-17:30']),
            'tuesday'    : Tuesday open hours (list of strings),
            'wednesday'  : Wednesday open hours (list of strings),
            'thursday'   : Thursday open hours (list of strings),
            'friday'     : Friday open hours (list of strings),
            'saturday'   : Saturday open hours (list of strings),
            'sunday'     : Sunday open hours (list of strings)
        },
        'human_friendly_open_hours': {
            // Different from open_hours for processing after midnight hours
            // e.g : For opening on Monday from 11:00 to 02:00 => 'monday': ['11:00-02:00+']
            'monday'     : Monday open hours (list of strings like ['07:30-10:00', '13:30-17:30']),
            'tuesday'    : Tuesday open hours (list of strings),
            'wednesday'  : Wednesday open hours (list of strings),
            'thursday'   : Thursday open hours (list of strings),
            'friday'     : Friday open hours (list of strings),
            'saturday'   : Saturday open hours (list of strings),
            'sunday'     : Sunday open hours (list of strings)
        },
        'specific_hours'  : {
            'open': [
                {
                    'starts_at': Start date of specific open hours (string like <YYYY>-<MM>-<DD>),
                    'ends_at': End date of specific open hours (string like <YYYY>-<MM>-<DD>),
                    'open_hours': list of strings like ['07:30-10:00', '13:30-17:30']
                }
            ],
            'close': [
                {
                    'starts_at': Start date of specific closed days (string like <YYYY>-<MM>-<DD>),
                    'ends_at': End date of specific closed days (string like <YYYY>-<MM>-<DD>),
                }
            ]

        },
        'price_range': {
            'max'                 : upper price in cents (int),
            'min'                 : lower price in cents (int)
        },
        'promos'      : list of on-going promotional offer objects (see below) (list),
        'news'                    : list of news published by the business (see below) (list)
    }

Promotional offers:

.. code-block:: javascript

    {
        'title'          : title of the promotional offer (string),
        'description'    : description of the promotional offer (string),
        'created'        : timestamp when the promotional offer was created
        'begin'          : timestamp when the offer begins (int),
        'end'            : timestamp when the offer ends (int)
    }

News:

.. code-block:: javascript

    {
        'title'          : title of the promotional offer (string),
        'description'    : description of the promotional offer (string),
        'created'        : timestamp when the news was published (int),
    }


Business search
---------------------------------

The ``business/search/`` resource lets you search for businesses using filters. This resource also enables you to crawl the entire businesses database when no filter is applied.

Request URL:

.. code-block:: html

    GET http://www.partoo.fr/api/business/search/?api_key={my_api_key}

Request parameters:

==================  ==========  ===========  ===========    ==================
Parameter           Type        Required     Default        Description
==================  ==========  ===========  ===========    ==================
api_key             string      True         None           Your API key
org_id              int         False        None           Filter businesses by org id
name                string      False        None           Filter businesses by name
page                int         False        1              Page number. **30 results per page**.
country             string      False        None           Filter businesses by country - use ISO 3166 alpha2 code (i.e. use "FR" for France)
status              string      False        'open'         Filter businesses by status
timestamp           int         False        0              Filter businesses by modified attribute (modified >= timestamp)

==================  ==========  ===========  ===========    ==================

JSON response:

.. code-block:: javascript

    {
        'page'            : page number (int),
        'max_page'        : last page number (int),
        'count'           : total number of businesses found (int),
        'businesses': [
                        list of business objects (see the business details resource)
        ]
    }



Business event
---------------------------------

When a business is **created**, **updated** or **deleted**, it generates an "event" on our side. When a business is updated or created, the event contains the business fields that were updated (or just filled in for the first time in the case of a business creation) with their new value.

Request URL:

.. code-block:: html

    GET http://www.partoo.fr/api/event/{event_id}?api_key={my_api_key}


Request parameters:

==================  ==========  ===========  ==================
Parameter           Type        Required     Description
==================  ==========  ===========  ==================
api_key             string      True         Your API key
event_id            string      True         Id of the event
==================  ==========  ===========  ==================

JSON response:

.. code-block:: javascript

    {
        'id'                : event id (string),
        'business_id'       : business id (string),
        'org_id'            : org id (int),
        'is_entreprise'     : (bool),
        'timestamp'         : timestamp when the event was created (float),
        'type'              : type of event (creation, update, deletion) (string),
        'business': {
                            business fields that were updated. See the Business details resource
                            to see what fields can be there.
        }
    }


In the example below, the event reports that the name and logo URL of this business were updated and gives the new values.

.. code-block:: javascript

    {
        id: "53a74fb5ffb44411ed345c59",
        business_id: "53a6f5daffb4440ec5ffbf95",
        timestamp: 1403473845,
        type: "update",
        business: {
            name: "La bonne blanquette",
            logo_url: "http://www.labonneblanquette.com/logo.png"
        }
    }



Business event search
---------------------------------

The ``event/search/`` resource lets you search for business update events. It basically enables you to know when our database is updated and what information are updated. You should use this resource to update businesses information on a regular basis rather than querying for all information of every single business.

Request URL:

.. code-block:: html

    GET http://www.partoo.fr/api/event/search/?api_key={my_api_key}


Request parameters:

==================  ==========  ===========  ===========  ==================
Parameter           Type        Required     Default      Description
==================  ==========  ===========  ===========  ==================
api_key             string      True         None         Your API key
org_id              int         False        None         Get events only for a given org
type                string      False        ''           Filter events by type : creation, update, deletion
after               int         False        0            Get events that were created after this timestamp
before              int         False        None         Get events that were created before this timestamp
page                int         False        1            Page number. **30 results per page**.
status              string      False        None         Filter events by business status

==================  ==========  ===========  ===========  ==================

JSON response:

.. code-block:: javascript

    {
        'page'            : page number (int),
        'max_page'        : last page number (int),  
        'count'           : total number of business events found (int),
        'events': [
                        list of business event objects
        ]
    }


Example:

If I want to get all updates made on the businesses for org 2 after the 11th of November 2014, I will use the following request:

.. code-block:: html

    GET http://www.partoo.fr/api/event/search/?api_key={my_api_key}&org_id=2&type=update&after=1415660400


Organisation details
---------------------------------

Retrieve **information** about a given **organisation**.

Request URL:

.. code-block:: html

    GET http://www.partoo.fr/api/org/{org_id}?api_key={my_api_key}


Query parameters:

==================  ==========  ===========  ======================
Parameter           Type        Required     Description
==================  ==========  ===========  ======================
api_key             string      True         Your API key
org_id              string      True         Id of the organisation
==================  ==========  ===========  ======================

JSON response:

.. code-block:: javascript

    {
        'org_id'        : organisation id (int),
        'name'          : name of the organisation (string),
        'created'       : timestamp when the organisation was created on Partoo (int)
    }




