.. Partoo API documentation documentation master file, created by
   sphinx-quickstart on Sat Jul 12 04:06:18 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Partoo API documentation
==========================================

Welcome on Partoo API documentation website! Partoo works hard on **gathering rich and up-to-date information** about **local businesses** in a structured format. We collect rich contents such as restaurants' menus, special offers, photographies and pictures, opening hours and so on. Our database keeps growing every day!

Our goal is to share all these information with you and continue **helping local businesses** to be present all over the internet.

By using our API, **you agree with our terms of use** : http://www.partoo.fr/cgu-api.

Table of contents
==========================================

.. toctree::
   :maxdepth: 2

   getting_started
   resources
   help

